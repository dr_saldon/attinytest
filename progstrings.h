/*
 * progstrings.h
 *
 * Created: 24.11.2017 9:08:50
 *  Author: Dr. Saldon
 */ 

#ifndef PROGSTRINGS_H_
#define PROGSTRINGS_H_

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include <avr/pgmspace.h>
#include "uart.h"

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
/**
 * Macro for sending progstrings over UART
 */
#define SEND_STR_P(buff, array, str) {strcpy_P(buff, (PGM_P)pgm_read_word(&(array[str]))); send_uart_str(buff);}

/**
 *	Progstring names
 */
#define RET				0
#define RUN				1
#define HELLO			2

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
/**
 * Prepare your program strings here
 */
const char ret[] PROGMEM = "\r\n";
const char run[] PROGMEM = "Run!\r\n";
const char hello[] PROGMEM = "Hello =)\r\n";

/**
 * Make an array using strings, defined in previous block
 * Use SEND_STR_P macro when you want to send your string over UART
 * Example:
 *	SEND_STR_P(uart_buffer, strings, RET);
 */
PGM_P const strings[] PROGMEM = {
	ret,
	run,
hello};

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/

#endif /* PROGSTRINGS_H_ */