/*
 * seven_seg.h
 *
 * Created: 16.11.2017 23:10:56
 *  Author: Dr. Saldon
 */ 

#ifndef SEVEN_SEG_H_
#define SEVEN_SEG_H_

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include <avr/io.h>

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
/**
 *  Bit definitions for CA 7-segment LED GNS-3013BD (only for my board)
 * Q0 - F (2)
 * Q1 - A (10)
 * Q2 - B (9)
 * Q3 - C (8)
 * Q4 - DP (7)
 * Q5 - D (5)
 * Q6 - E (4)
 * Q7 - G (3)
 */
#define N_NAN	0b11111111
#define N0		0b10010000
#define N1		0b11110011
#define N2		0b00011001
#define N3		0b01010001
#define N4		0b01110010
#define N5		0b01010100
#define N6		0b00010100
#define N7		0b11110001
#define N8		0b00010000
#define N9		0b01010000
#define DP		0b11101111

#define NA		0b00110000
#define NB		0b00010110
#define NC		0b10011100
#define ND		0b00010011
#define NE		0b01011100
#define NF		0b00111100
#define NH		0b00110010
#define NL		0b10011110
#define NN		0b00110111
#define NO		0b10010000
#define NP		0b00111000
#define NR		0b10111100
#define NS		0b01010100
#define NT		0b00010110
#define NU		0b10010010
#define NY		0b01010010

#define HC595_PORT   PORTB
#define HC595_DDR    DDRB

#define HC595_DS_POS PB2			//Data pin (DS) pin location

#define HC595_SH_CP_POS PB4			//Shift Clock (SH_CP) pin location 
#define HC595_ST_CP_POS PB3			//Store Clock (ST_CP) pin location

//Low level macros to change data (DS)lines
#define HC595DataHigh() (HC595_PORT|=(1<<HC595_DS_POS))
#define HC595DataLow() (HC595_PORT&=(~(1<<HC595_DS_POS)))

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
void HC595Init(void);
void HC595Pulse(void);
void HC595Latch(void);
void HC595Write(uint8_t data);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/

//Initialize HC595 System
void HC595Init(void)
{
   //Make the Data(DS), Shift clock (SH_CP), Store Clock (ST_CP) lines output
   HC595_DDR|=((1<<HC595_SH_CP_POS)|(1<<HC595_ST_CP_POS)|(1<<HC595_DS_POS));
}

//Sends a clock pulse on SH_CP line
void HC595Pulse(void)
{
   //Pulse the Shift Clock
   HC595_PORT|=(1<<HC595_SH_CP_POS);//HIGH
   HC595_PORT&=(~(1<<HC595_SH_CP_POS));//LOW
}

//Sends a clock pulse on ST_CP line
void HC595Latch(void)
{
   //Pulse the Store Clock
   HC595_PORT|=(1<<HC595_ST_CP_POS);      //HIGH
   _delay_loop_1(1);
   HC595_PORT&=(~(1<<HC595_ST_CP_POS));   //LOW
   _delay_loop_1(1);
}

/**
 * Main High level function to write a single byte to
 * Output shift register 74HC595. 
 * 
 * Arguments:
 *    single byte to write to the 74HC595 IC
 * 
 * Returns:
 *    NONE
 * 
 * Description:
 *    The byte is serially transfered to 74HC595
 *    and then latched. The byte is then available on
 *    output line Q0 to Q7 of the HC595 IC.
 * 
 */
void HC595Write(uint8_t data)
{
   //Send each 8 bits serially
   //Order is MSB first
   for(uint8_t i=0;i<8;i++)
   {
      //Output the data on DS line according to the
      //Value of MSB
      if(data & 0b10000000)
      {
         HC595DataHigh();		//MSB is 1 so output high
      }
      else
      {
         HC595DataLow();		//MSB is 0 so output high
      }

      HC595Pulse();  //Pulse the Clock line
      data=data<<1;  //Now bring next bit at MSB position
   }
   //Now all 8 bits have been transferred to shift register
   //Move them to output latch at one
   HC595Latch();
}

void HC595WriteDigit(uint8_t digit)
{
	uint8_t _tmp = 0;
	switch(digit)
	{
	case 0:
	{
		_tmp = N0;
		break;
	}
	case 1:
	{
		_tmp = N1;
		break;
	}
	case 2:
	{
		_tmp = N2;
		break;
	}
	case 3:
	{
		_tmp = N3;
		break;
	}
	case 4:
	{
		_tmp = N4;
		break;
	}
	case 5:
	{
		_tmp = N5;
		break;
	}
	case 6:
	{
		_tmp = N6;
		break;
	}
	case 7:
	{
		_tmp = N7;
		break;
	}
	case 8:
	{
		_tmp = N8;
		break;
	}
	case 9:
	{
		_tmp = N9;
		break;
	}
	default:
	{
		_tmp = N_NAN;
		break;
	}
	}
	HC595Write(_tmp);
}

#endif /* SEVEN_SEG_H_ */