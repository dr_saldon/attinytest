/*
 * ATTinytest.c
 *
 * Created: 13.11.2017 8:49:13
 * Author : Dr. Saldon
 */ 

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
#define F_CPU 8000000UL		// CPU clock speed without external crystal = 1000000UL

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include <inttypes.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>
#include "progstrings.h"
#include "a2313_common_helpers.h"
#include "uart.h"
#include "seven_seg.h"
#include "avr_fifo.h"

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
volatile uint32_t loop1_timer = 0;	// task loop timer
uint8_t disp_counter = 0;

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
void parse_input(char *data);
void loop_idle(void);
void loop1(void);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/
void parse_input(char *data)
{
	uint8_t in_tmp = 0;
	in_tmp = atoi((const char*)data);
	if((in_tmp > 0) && (in_tmp < 10))
	{
		disp_counter = in_tmp;
		HC595WriteDigit(disp_counter);
	} 
}

void loop_idle(void)
{
	if(fifo_flag)
	{
		char data;
		if(rx_FIFO_get(&data))
		{
			parse_input(&data);
		}
	}
}

void loop1(void)
{
	// utoa(integer_to_convert, char_buffer_pointer, base); where base can be 10 (decimal), 16 (hex), 8 (octal), 2 (binary)	
	utoa(millis(), uart_buffer, 10);	
	send_uart_str(uart_buffer);
	SEND_STR_P(uart_buffer, strings, RET);
	SEND_STR_P(uart_buffer, strings, HELLO);

	if (disp_counter <= 9)
	{
		HC595WriteDigit(disp_counter);
		disp_counter++;
		if (disp_counter == 10)
		{
			disp_counter = 0;
		}
	}
}

ISR(USART_RX_vect)
{
	char rx_tmp;
	rx_tmp = UDR;
	fifo_flag = rx_FIFO_put(rx_tmp);
}

/*-----------------------------------------------------------------------------
ENTRY POINT
-----------------------------------------------------------------------------*/
int main(void)
{
    init_uart();			// Initialize UART (baud rate BAUD)
	init_timer0();			// Initialize TIMER0 (SysTick analog for millis())
	HC595Init();			// Initialize 7-segment LED indicator
	rx_FIFO_init();
	tx_FIFO_init();

	SEND_STR_P(uart_buffer, strings, RUN);

	loop1_timer = millis();

    while (1) 
    {
		loop_idle();

		if ((millis() - loop1_timer) > 1000)
		{
			loop1_timer = millis();
			loop1();
		}
    }
}

