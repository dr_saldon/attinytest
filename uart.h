/*
 * uart.h
 *
 * Created: 13.11.2017 10:00:19
 *  Author: Dr. Saldon
 */ 


#ifndef UART_H_
#define UART_H_

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
#ifndef F_CPU
	#define F_CPU 8000000UL		// CPU clock speed without external crystal = 1000000UL
#endif

#define BAUD 38400
#define UBBRVAL ((F_CPU / (BAUD * 16L)) - 1)
#define BUFFER_SIZE 16

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
char uart_buffer[BUFFER_SIZE];	// Used as progstrings template storage and for conversion from integer to string

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
void init_uart(void);
void send_uart_byte(char c);
void send_uart_str(char* str);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/
/*
 * init_uart
 */
void init_uart(void) {
  // set baud rate
  UBRRH = (uint8_t)(UBBRVAL >> 8); 
  UBRRL = (uint8_t)(UBBRVAL);
  // enable receive and transmit
  UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
  // set frame format
  UCSRC = (1 << USBS) | (3 << UCSZ0);	// asynchron 8n1
  sei();
}

/*
 * send_uart
 * Sends a single char to UART without ISR
 */
void send_uart_byte(char c) {
	while (!(UCSRA & (1<<UDRE)));		// wait for empty data register
	UDR = c;							// set data into data register
}

void send_uart_str(char* str) {
	while(*str) {
		send_uart_byte(*str++);
	}
}

#endif /* UART_H_ */