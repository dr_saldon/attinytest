/*
 * avr_semaphore.h
 *
 * Created: 23.11.2017 19:30:02
 *  Author: Dr. Saldon
 */ 

#ifndef AVR_SEMAPHORE_H_
#define AVR_SEMAPHORE_H_

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
typedef uint8_t semaphore_t;

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
uint8_t semaphoreTake(semaphore_t *semaphore);
uint8_t semaphoreGive(semaphore_t *semaphore);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/
/**
 * Take Semaphore
 */
uint8_t semaphoreTake(semaphore_t *semaphore)
{
	if (!*semaphore)
	{
		*semaphore = 1;
		return 1;
	}
	return 0;
}

/**
 * Give Semaphore
 */
uint8_t semaphoreGive(semaphore_t *semaphore)
{
	if (*semaphore)
	{
		*semaphore = 0;
		return 1;
	}
	return 0;
}

#endif /* AVR_SEMAPHORE_H_ */