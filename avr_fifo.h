/*
 * avr_fifo.h
 *
 * Created: 23.11.2017 7:14:48
 *  Author: Dr. Saldon
 */ 

#ifndef AVR_FIFO_H_
#define AVR_FIFO_H_

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
#define TX_FIFO_SIZE    4
#define RX_FIFO_SIZE    16
#define FIFO_SUCCESS	1
#define FIFO_FAIL		0

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include "avr_semaphore.h"

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
static char rx_buffer[RX_FIFO_SIZE];
static char *rx_head;
static char *rx_tail;
semaphore_t rx_semaphore;

static char tx_buffer[TX_FIFO_SIZE];
static char *tx_head = NULL;
static char *tx_tail = NULL;
semaphore_t tx_semaphore;

volatile uint8_t fifo_flag = 0;

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
void rx_FIFO_init(void);
uint8_t rx_FIFO_empty(void);
uint8_t rx_FIFO_full(void);
uint8_t rx_FIFO_put(char c);
uint8_t rx_FIFO_get(char *c);
void tx_FIFO_init(void);
uint8_t tx_FIFO_empty(void);
uint8_t tx_FIFO_full(void);
uint8_t tx_FIFO_put(char c);
uint8_t tx_FIFO_get(char *c);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/
void rx_FIFO_init(void)
{
	rx_head = rx_tail = rx_buffer;
	semaphoreGive(&rx_semaphore);
}

uint8_t rx_FIFO_empty(void)
{
	return rx_head == rx_tail;
}

uint8_t rx_FIFO_full(void)
{
	char *p = rx_tail + 1;
	if (p == rx_buffer + RX_FIFO_SIZE)         //  if (p == &buffer[FIFO_SIZE])
	p = rx_buffer;                      //      p = &buffer[0];
	return p == rx_head;
}

uint8_t rx_FIFO_put(char c)
{
	if (semaphoreTake(&rx_semaphore))
	{
		if (rx_FIFO_full())
		{
			semaphoreGive(&rx_semaphore);
			return FIFO_FAIL;
		}
		*rx_tail++ = c;
		if (rx_tail == rx_buffer + RX_FIFO_SIZE)   //  if (tail == &buffer[FIFO_SIZE])
		rx_tail = rx_buffer;                //      tail = &buffer[0];
		semaphoreGive(&rx_semaphore);
		return FIFO_SUCCESS;
	}
	return FIFO_FAIL;
}

uint8_t rx_FIFO_get(char *c)
{
	if (semaphoreTake(&rx_semaphore))
	{
		if (rx_FIFO_empty())
		{
			semaphoreGive(&rx_semaphore);
			return FIFO_FAIL;
		}
		*c = *rx_head++;
		if (rx_head == rx_buffer + RX_FIFO_SIZE)   //  if (head == &buffer[FIFO_SIZE])
		rx_head = rx_buffer;                //      head = &buffer[0];
		semaphoreGive(&rx_semaphore);
		return FIFO_SUCCESS;
	}
	return FIFO_FAIL;
}

void tx_FIFO_init(void)
{
	tx_head = tx_tail = tx_buffer;
	semaphoreGive(&tx_semaphore);
}

uint8_t tx_FIFO_empty(void)
{
	return tx_head == tx_tail;
}

uint8_t tx_FIFO_full(void)
{
	char *p = tx_tail + 1;
	if (p == tx_buffer + TX_FIFO_SIZE)         //  if (p == &buffer[FIFO_SIZE])
	p = tx_buffer;                      //      p = &buffer[0];
	return p == tx_head;
}

uint8_t tx_FIFO_put(char c)
{
	if (semaphoreTake(&tx_semaphore))
	{
		if (tx_FIFO_full())
		{
			semaphoreGive(&tx_semaphore);
			return FIFO_FAIL;
		}
		*tx_tail++ = c;
		if (tx_tail == tx_buffer + TX_FIFO_SIZE)   //  if (tail == &buffer[FIFO_SIZE])
		tx_tail = tx_buffer;                //      tail = &buffer[0];
		semaphoreGive(&tx_semaphore);
		return FIFO_SUCCESS;
	}
	return FIFO_FAIL;
}

uint8_t tx_FIFO_get(char *c)
{
	if (semaphoreTake(&tx_semaphore))
	{
		if (tx_FIFO_empty())
		{
			semaphoreGive(&tx_semaphore);
			return FIFO_FAIL;
		}
		*c = *tx_head++;
		if (tx_head == tx_buffer + TX_FIFO_SIZE)    //  if (head == &buffer[FIFO_SIZE])
		tx_head = tx_buffer;                 //      head = &buffer[0];
		semaphoreGive(&tx_semaphore);
		return FIFO_SUCCESS;
	}
	return FIFO_FAIL;
}

#endif /* AVR_FIFO_H_ */