/*
 * a2313_common_helpers.h
 *
 * Created: 14.11.2017 0:25:13
 *  Author: Dr. Saldon
 */ 

#ifndef A2313_COMMON_HELPERS_H_
#define A2313_COMMON_HELPERS_H_

/*-----------------------------------------------------------------------------
MACRO SECTION
-----------------------------------------------------------------------------*/
#ifndef F_CPU
#define F_CPU 8000000UL		// CPU clock speed without external crystal = 1000000UL
#endif

/*-----------------------------------------------------------------------------
INCLUDE SECTION
-----------------------------------------------------------------------------*/
#include <avr/io.h>
#include <avr/interrupt.h>

/*-----------------------------------------------------------------------------
GLOBAL VARIABLES SECTION
-----------------------------------------------------------------------------*/
volatile uint32_t _millis = 0;
volatile uint16_t _us = 0;

/*-----------------------------------------------------------------------------
HEADER SECTION
-----------------------------------------------------------------------------*/
void init_timer0(void);
void tim_handler(void);
uint32_t millis(void);

/*-----------------------------------------------------------------------------
IMPLEMENTATION SECTION
-----------------------------------------------------------------------------*/
void init_timer0(void) {
	TIMSK |= (1 << TOIE0);		// Enable timer interrupt
	TCNT0 |= 0x00;				// Set initial timer value
	TCCR0B |= (1 << CS01);		// Set prescaler = 8
	sei();						// Enable interrupts
}

void tim_handler(void) {
	_us += 256;
	while (_us >= 1000) {
		_millis++;
		_us -= 1000;
	}
}

uint32_t millis(void) {
	uint32_t m;
	cli();
	m = _millis;
	sei();
	return m;
}

/*-----------------------------------------------------------------------------
ISR SECTION
-----------------------------------------------------------------------------*/
ISR(__vector_default){}

ISR(TIMER0_OVF_vect)
{
	tim_handler();
}

#endif /* A2313_COMMON_HELPERS_H_ */